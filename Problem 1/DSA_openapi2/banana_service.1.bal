import  ballerina/http;
import ballerina/io;
//a

listener  http:Listener  ep0  = new (9090, config  = {host: "localhost"});

type subjects record {|
    string course;
    string score;
|};

type Profile record {|
    string username;
    string lastname;
    string firstname;
    
    string[] preferred_formats;
    subjects[] past_subjects;
    

|};


type Materials record {|
    string course;
    Material_Learning_Objects learning_objects;
|};

type Material_Learning_Objects record {|
    Material_Required required;
    Material_Suggested suggested;

|};

type Material_Audio record{|
    string name;
    string description;
    string difficulty;
    
|};

type Material_text record {|
    string mat_text;

|};

type Material_Required record {|

    Material_Audio[] audio;
    Material_text[] text;

|};

type Material_Suggested record {|
    string[] video;
    string[] audio;

|};


Materials easyMaterials = {course: "Distributed Systems", learning_objects: {
    required: {
        audio: [{name: "Topic 1",description: "Basics of Distributed Systems",difficulty: "Easy"}, 
        {name: "Topic 2",description: "Basics of Distributed Systems",difficulty: "Easy"},
        {name: "Topic 3",description: " Distributed Systems",difficulty: "Medium"},
        {name: "Topic 4",description: " Distributed Systems",difficulty: "Medium"},
        {name: "Topic 5",description: " Distributed Systems",difficulty: "Advanced"}
        ],
        text: []
    },
    suggested: {
        video: [
        "Distributed Systems Explained in 10 Minutes", "Distributed Systems for beginners", "Microservices Explained", "Distributed Systems Explaiend"],
        audio: []
    }
}};

Materials mediumMaterials = {course: "Distributed Systems", learning_objects: {
    required: {
        audio: [{name: "Topic 3",description: " Distributed Systems",difficulty: "Medium"}, 
        {name: "Topic 4",description: "Microservices and importance",difficulty: "Medium"},
        {name: "Topic 5",description: " Distributed Systems",difficulty: "Advanced"}],
        text: []
    },
    suggested: {
        video: ["Microservices Explained","Distributed Systems Explained"],
        audio: []
    }
}};

Materials advancedMaterials = {course: "Distributed Systems", learning_objects: {
    required: {
        audio: [{name: "Topic 5",description: "Distributed Systems Advanced",difficulty: "Advanced"}
        ],
        text: []
    },
    suggested: {
        video: ["Microservices Explained"],
        audio: []
    }
}};

Profile[] learner_profile = [];




 service  /LearnerProfiles  on  ep0  {
        resource  function  get  GetAllProfile()  returns  Profile[] {

            return learner_profile;
    }


        resource  function  get  GetProfile/[int  profile_id]()  returns   Profile|http:Unauthorized|Materials|json {
            
        Materials resp = easyMaterials;    
   
    if (learner_profile[profile_id].past_subjects[0].score == "A+" || learner_profile[profile_id].past_subjects[0].score == "A" ||learner_profile[profile_id].past_subjects[1].score == "A+" || learner_profile[profile_id].past_subjects[1].score == "A") {
        resp =  advancedMaterials;
        io:println("Advanced");
    } else if  (learner_profile[profile_id].past_subjects[0].score == "B" || learner_profile[profile_id].past_subjects[0].score == "C" || learner_profile[profile_id].past_subjects[1].score == "B" || learner_profile[profile_id].past_subjects[1].score == "B") {
       resp = mediumMaterials;
       io:println("Medi");
      
    } else {
        resp = easyMaterials;
        io:println("Easy");
    }

  

         return resp;
            
            
            
    }
        resource  function  put  UpdateProfile/[int  profile_id](@http:Payload  Profile  payload)  returns  http:Ok|http:BadRequest| string| Profile | json {
            learner_profile[profile_id].firstname = payload.firstname; 
            learner_profile[profile_id].lastname = payload.lastname;
            learner_profile[profile_id].past_subjects = payload.past_subjects;
            learner_profile[profile_id].preferred_formats = payload.preferred_formats;
            learner_profile[profile_id].username = payload.username;

           return {Done: "Profile has been Successfully Updated"};
    }
        resource  function  post  CreateProfile(@http:Payload  Profile  payload)  returns  string| http:Ok | json  {
            learner_profile.push(payload);
            return {Done: "Profile has been Successfully Created"};
            
    }
}
