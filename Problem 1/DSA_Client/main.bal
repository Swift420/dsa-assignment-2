import ballerina/http;
import ballerina/io;
import ballerina/lang.'int as ints;





final http:Client clientEndpoint = check new ("http://localhost:9090/LearnerProfiles");


function GetRequest() returns error?{
    io:print("");
    string count = io:readln("Enter ID of the Learner Profile: ");
    int res2 = check ints:fromString(count);
    json resp = check clientEndpoint->get("/GetProfile/"+res2.toString());
    io:println(resp.toJsonString());
    io:println("");
}

function GetAllProfileRequest() returns error?{
    io:println("");
    json resp = check clientEndpoint->get("/GetAllProfile");
    io:println(resp.toJsonString());
    io:println("");
}

function CreateProfileRequest() returns error?{
    io:println("");
    string username = io:readln("Enter Username: ");
    string firstname = io:readln("Enter Firstname: ");
    string lastname = io:readln("Enter lastname: ");
    string pref1 = io:readln("Enter Preferred Formats 1(Leave blank if you have none): ");
    string pref2 = io:readln("Enter Preferred Formats 2(Leave blank if you have none): ");
    string pref3 = io:readln("Enter Preferred Formats 3(Leave blank if you have none): ");
    string course1 = io:readln("Past Subject Course 1: ");
    string score1 = io:readln("Past Subject Score 1: ");
    string course2 = io:readln("Past Subject Course 2: ");
    string score2 = io:readln("Past Subject Score 2: ");


      io:println("\nPOST request:");
            json resp = check clientEndpoint->post("/CreateProfile", {"username": username,  "lastname": lastname,  "firstname": firstname,  "preferred_formats": [    pref1, pref2, pref3  ],  "past_subjects": [    {      "course": course1,      "score": score1    },{      "course": course2,      "score": score2    }  ]});
            io:println(resp.toJsonString());

         io:println("");   
    
}


function UpdateProfileRequest() returns error?{
    io:println("");
    string id = io:readln("Enter Profile ID you wish to Edit: ");
    int res3 = check ints:fromString(id);
    string username = io:readln("Enter Username: ");
    string firstname = io:readln("Enter Firstname: ");
    string lastname = io:readln("Enter lastname: ");
    string pref1 = io:readln("Enter Preferred Formats 1(Leave blank if you have none): ");
    string pref2 = io:readln("Enter Preferred Formats 2(Leave blank if you have none): ");
    string pref3 = io:readln("Enter Preferred Formats 3(Leave blank if you have none): ");
    string course1 = io:readln("Past Subject Course 1: ");
    string score1 = io:readln("Past Subject Score 1: ");
    string course2 = io:readln("Past Subject Course 2: ");
    string score2 = io:readln("Past Subject Score 2: ");


      io:println("\nPOST request:");
            json resp = check clientEndpoint->put("/UpdateProfile/"+res3.toString(), {"username": username,  "lastname": lastname,  "firstname": firstname,  "preferred_formats": [    pref1, pref2, pref3  ],  "past_subjects": [    {      "course": course1,      "score": score1    },{      "course": course2,      "score": score2    }  ]});
            io:println(resp.toJsonString());

         io:println("");   
    
}





public function main() returns error? {
    
    

    boolean cont = true;

    while cont==true {
        io:println("Choose one of the Following Options: ");
        io:println("1. Get All Learner Profiles (GET)");
        io:println("2. Get A Single Learner's Learning Material (GET)");
        io:println("3. Create a Learner Profile (POST)");
        io:println("4. Update a Learner Profile (PUT)");
        string ans = io:readln("Which Option Would you like: ");
        io:println("");
        int res1 = check ints:fromString(ans);
        if  res1 == 1 {
            io:println(GetAllProfileRequest());
            
        } else if res1==2{
            io:println(GetRequest());
            
        } else if res1==3 {
            
            io:println(CreateProfileRequest());
            
        } else if res1==4{
            io:println(UpdateProfileRequest());
            
        } else {
            io:println("Please pick number from 1-4");
            cont=true;
        }
        io:print("");
        string answer1 = io:readln("Do you want to call another function? y/n: ");

        if answer1 == "y" {
            cont = true;
        } else {
            cont = false;
        }

        

    }

    
        




    
}
//curl -X 'PUT' \  'http://localhost:9090/LearnersProfile/UpdateProfile/1' \  -H 'accept: */*' \  -H 'Content-Type: application/json' \  -d '{  "username": "Swift",  "lastname": "John",  "firstname": "Doe",  "preferred_formats": [    "John Doe"  ],  "past_subjects": [    {      "course": "string",      "score": "string"    }  ]}'
